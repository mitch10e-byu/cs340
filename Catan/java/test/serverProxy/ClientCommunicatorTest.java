package serverProxy;

import client.communication.IServerPoller;
import client.communication.ServerPoller;
import client.model.ClientModel;
import client.model.IClientModel;
import com.google.gson.JsonObject;
import org.junit.*;
import static org.junit.Assert.*;

import serverProxy.ClientCommunicator;
import serverProxy.response.FailSuccessResponse;
import serverProxy.response.GameListResponse;
import serverProxy.response.GameResponse;
import utils.GSONFileImport;

import java.util.List;

public class ClientCommunicatorTest {

    private ClientCommunicator communicator;

    @BeforeClass
    public static void setUp() {
        System.out.println("[INFO]\tRunning ClientCommunicatorTest");
    }

    @Before
    public void createEnvironment() {
        IServer server = new ServerProxy("http://localhost", Integer.parseInt("8081"));

        JsonObject clientJson = GSONFileImport.convertFileToJSON("jsonExamples/testModel.json");
        JsonObject cookie = GSONFileImport.convertFileToJSON("jsonExamples/testCookie.json");
        IClientModel clientModel = new Translator().translate(clientJson, cookie);

        communicator = new ClientCommunicator(server);
    }

    @Test
    public void testRegisterUser() {
        try {
            boolean r1 = communicator.registerUser("jacob", "glad");
            boolean r2 = communicator.registerUser("jacob", "glad");
            boolean r3 = communicator.registerUser("jacob", "sad");
            boolean r4 = communicator.registerUser("other", "guy");
            boolean r5 = communicator.registerUser("third", "guy");
            boolean r6 = communicator.registerUser("fourth", "guy");

            assertTrue(r1);
            assertTrue(r4);
            assertTrue(r5);
            assertTrue(r6);

            assertFalse(r2);
            assertFalse(r3);
        } catch (Exception e) {
            System.out.println("Test Cases failed: " + e.getMessage());
        }
    }

    @Test
    public void testLoginUser() {
        try {
            boolean l1 = communicator.loginUser("Sam", "sam");
            boolean l2 = communicator.loginUser("FAIL", "fail");

            assertTrue(l1);
            assertFalse(l2);

        } catch (Exception e) {
            System.out.println("Test Cases failed: " + e.getMessage());
        }
    }

    @Test
    public void testListGames() {
        try {
            List<GameResponse> games = communicator.listGames();

            assertEquals(games.get(0).getTitle(), "Default Game");
            assertEquals(games.get(1).getTitle(), "AI Game");
            assertEquals(games.get(2).getTitle(), "Empty Game");

        } catch (Exception e) {
            System.out.println("Test Cases failed: " + e.getMessage());
        }
    }

    @Test
    public void testCreateGame() {
        try {
            GameResponse response = communicator.createGame("TestCreateGame", true, true, true);
            assertEquals(response.getTitle(), "TestCreateGame");
        } catch (Exception e) {
            System.out.println("Test Cases failed: " + e.getMessage());
        }
    }

    @Test
    public void testJoinGame() {
        try {
            boolean response = communicator.joinGame(0, "red");
//            Returns false without cookie set...
            assertFalse(response);
        } catch (Exception e) {
            System.out.println("Test Cases failed: " + e.getMessage());
        }
    }


}
