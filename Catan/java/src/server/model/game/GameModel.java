package server.model.game;

import server.model.map.*;
import server.model.message.IMessageList;
import server.model.message.MessageList;
import shared.definitions.CatanColor;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GameModel implements IGameModel {

	private static GameModel updatableModel = null;

	private ITurnTracker turnTracker;
	private int winner;
	private int version;
	private IMessageList chat;
	private IMessageList log;
	private ICatanMap catanMap;
	private List<IPlayer> players;
	private IResourceList bank;
	private IBonusTracker bonusTracker;
	private ITradeOffer tradeOffer;
	private IDevCardList deck;
	
	private String title;
	private int id;

	public GameModel(String title, int id, boolean randomTiles, boolean randomNumbers, boolean randomPorts) {
		winner = -1;
		version = 0;
		turnTracker = new TurnTracker();
		chat = new MessageList();
		log = new MessageList();
		bank = new ResourceList();
		bonusTracker = new BonusTracker();
		players = new ArrayList<>();
		this.id = id;
		this.title = title;
	}

	public GameModel(ITurnTracker turnTracker, int winner, int version, IMessageList chat,
			IMessageList log, ICatanMap catanMap, List<IPlayer> players, IResourceList bank,
			IBonusTracker bonusTracker, ITradeOffer tradeOffer, IPlayer localPlayer,
			IDevCardList deck) {
		super();
		this.turnTracker = turnTracker;
		this.winner = winner;
		this.version = version;
		this.chat = chat;
		this.log = log;
		this.catanMap = catanMap;
		this.players = players;
		this.bank = bank;
		this.bonusTracker = bonusTracker;
		this.tradeOffer = tradeOffer;
		this.deck = deck;
	}

	public static void setUpdatableModel(GameModel newModel) {
		updatableModel = newModel;
	}

	public static GameModel getUpdatableModel() {
		return updatableModel;
	}

	/**
	 * @.obviousGetter
	 */
	public ITurnTracker getTurnTracker() {
		return turnTracker;
	}

	/**
	 * @.obviousGetter
	 */
	public ICatanMap getCatanMap() {
		return catanMap;
	}

	/**
	 * @.obviousGetter
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * @.obviousGetter
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @.obviousGetter
	 */
	public IMessageList getChat() {
		return chat;
	}

	/**
	 * @.obviousGetter
	 */
	public IMessageList getLog() {
		return log;
	}

	/**
	 * @.obviousGetter
	 */
	public List<IPlayer> getPlayers() {
		return players;
	}

	/**
	 * @.obviousSetter
	 */
	public void setTurnTracker(ITurnTracker turnTracker) {
		this.turnTracker = turnTracker;
	}

	/**
	 * @.obviousSetter
	 */
	public void setWinner(int winner) {
		this.winner = winner;
	}

	/**
	 * @.obviousSetter
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @.obviousSetter
	 */
	public void setChat(IMessageList chat) {
		this.chat = chat;
	}

	/**
	 * @.obviousSetter
	 */
	public void setLog(IMessageList log) {
		this.log = log;
	}

	/**
	 * @.obviousSetter
	 */
	public void setCatanMap(ICatanMap catanMap) {
		this.catanMap = catanMap;
	}

	/**
	 * @.obviousSetter
	 */
	public void setPlayers(List<IPlayer> players) {
		this.players = players;
	}

	/**
	 * @.obviousGetter
	 */
	public IResourceList getBank() {
		return bank;
	}

	/**
	 * @.obviousSetter
	 */
	public void setBank(IResourceList bank) {
		this.bank = bank;
	}

	/**
	 * @.obviousGetter
	 */
	public IBonusTracker getBonusTracker() {
		return bonusTracker;
	}

	/**
	 * @.obviousSetter
	 */
	public void setBonusTracker(IBonusTracker bonusTracker) {
		this.bonusTracker = bonusTracker;
	}

	/**
	 * @.obviousGetter
	 */
	public ITradeOffer getTradeOffer() {
		return tradeOffer;
	}

	/**
	 * @.obviousSetter
	 */
	public void setTradeOffer(ITradeOffer tradeOffer) {
		this.tradeOffer = tradeOffer;
	}

	public IDevCardList getDeck() {
		return deck;
	}

	public void setDeck(IDevCardList deck) {
		this.deck = deck;
	}

	public boolean canRollNumber(int playerIndex) {
		// Not my turn
		if (turnTracker.getCurrentTurn() != playerIndex)
			return false;
		// has rolling as the status
		if (!turnTracker.getStatus().toLowerCase().equals("rolling"))
			return false;
		return true;
	}

	public boolean canAcceptTrade(int playerIndex) {
		if (tradeOffer == null)
			return false;
		if (tradeOffer.getReceiverId() != playerIndex)
			return false;

		if (!players.get(playerIndex).canAcceptTradeOffer(tradeOffer))
			return false;

		return true;
	}

	public boolean canOfferTrade(ITradeOffer tradeOffer, int playerIndex) {
		if (tradeOffer == null)
			return false;
		if (tradeOffer.getSenderId() != playerIndex)
			return false;
        if(!hasPositiveAndNegative(tradeOffer.getResources()))
            return false;
		if (!players.get(playerIndex).canOfferTradeOffer(tradeOffer))
			return false;
		return true;
	}
    private boolean hasPositiveAndNegative(IResourceList list) {
        boolean hasPositive = false;
        if(list.getQuantity(ResourceType.BRICK) > 0)
            hasPositive = true;
        if(list.getQuantity(ResourceType.WOOD) > 0)
            hasPositive = true;
        if(list.getQuantity(ResourceType.WHEAT) > 0)
            hasPositive = true;
        if(list.getQuantity(ResourceType.ORE) > 0)
            hasPositive = true;
        if(list.getQuantity(ResourceType.SHEEP) > 0)
            hasPositive = true;

        boolean hasNegative = false;
        if(list.getQuantity(ResourceType.BRICK) < 0)
            hasNegative = true;
        if(list.getQuantity(ResourceType.WOOD) < 0)
            hasNegative = true;
        if(list.getQuantity(ResourceType.WHEAT) < 0)
            hasNegative = true;
        if(list.getQuantity(ResourceType.ORE) < 0)
            hasNegative = true;
        if(list.getQuantity(ResourceType.SHEEP) < 0)
            hasNegative = true;

        return hasNegative && hasPositive;
    }
	public boolean canDiscardCards(IResourceList cards, int playerIndex) {
		if (cards == null)
			return false;
		if (players.get(playerIndex).isDiscarded())
			return false;

		if (!turnTracker.getStatus().toLowerCase().equals("discarding"))
			return false;

		IResourceList playerResources = players.get(playerIndex).getResources();
		int totalCards = playerResources.getTotalResourceCount();

		if (totalCards <= 7)
			return false;

		if (!playerResources.isGreaterOrEqualThanInput(cards))
			return false;

		return true;
	}

	public boolean canFinishTurn(int playerIndex) {
		// TODO: is this only true if the current player is playing???
		// Not my turn
		if (turnTracker.getCurrentTurn() != players.get(playerIndex).getPlayerIndex())
			return false;

		if (!turnTracker.getStatus().toLowerCase().equals("playing"))
			return false;

		return true;
	}

	public boolean canBuyDevCard(int playerIndex) {
		// TODO: can you buy during any part of the game??? should I check
		// status
		if (turnTracker.getCurrentTurn() != players.get(playerIndex).getPlayerIndex())
			return false;

		if (!players.get(playerIndex).canBuyDevCard())
			return false;

		if (deck.getTotalCardCount() < 1)
			return false;

		return true;
	}
    public Map<ResourceType, Integer> getTradeRatios(int playerIndex) {
        Map<ResourceType, Integer> ratios = new HashMap<>();
        ratios.put(ResourceType.BRICK, 4);
        ratios.put(ResourceType.ORE, 4);
        ratios.put(ResourceType.SHEEP, 4);
        ratios.put(ResourceType.WOOD, 4);
        ratios.put(ResourceType.WHEAT, 4);

        List<ICommunity> communities = new ArrayList<ICommunity>();
        communities.addAll(catanMap.getSettlements());
        communities.addAll(catanMap.getCities());
        for(IPort port : catanMap.getPorts()) {
            for(ICommunity community : communities){
            	if(community.getOwner().getPlayerIndex() == players.get(playerIndex).getPlayerIndex()) {
	                EdgeLocation portEdge = new EdgeLocation(port.getLocation(), port.getDirection());
	                if(LocationComparer.vertexIsOnEdge(portEdge, community.getLocation())){
	                    int portRatio = port.getRatio();
	                    ResourceType type = port.getResource();
	                    if(type == null) {
	                        for(ResourceType restype: ResourceType.values()) {
	                            if(ratios.get(restype) > portRatio)
	                                ratios.put(restype, 3);
	                        }
	                    }
	                    else if(ratios.get(type) > portRatio)
	                        ratios.put(type, portRatio);
	                }
            	}
            }
        }

        return ratios;
    }
	public boolean canMaritimeTrade(ResourceType inputResource, ResourceType outputResource,
			int ratio, int playerIndex) {
		if (ratio < 2 || ratio > 4)
			return false;
		boolean canMakeOffer = false;
		

		Map<ResourceType, Integer> ratios = new HashMap<>();
		ratios = getTradeRatios(playerIndex);
		if(ratios.get(inputResource)!=ratio)
			return false;
		
		ResourceList inputResources = new ResourceList();
		inputResources.setQuantity(inputResource, ratio);

		ResourceList outputResources = new ResourceList();
		outputResources.setQuantity(outputResource, 1);

		if (turnTracker.getCurrentTurn() != playerIndex)
			return false;
		if (!players.get(playerIndex).getResources().isGreaterOrEqualThanInput(inputResources))
			return false;
		if (!bank.isGreaterOrEqualThanInput(outputResources))
			return false;

		return true;
	}

	private boolean canPlayDevCard(DevCardType devCard, int playerIndex) {
		if (!turnTracker.getStatus().toLowerCase().equals("playing"))
			return false;
		if (turnTracker.getCurrentTurn() != playerIndex)
			return false;
		if (!players.get(playerIndex).canPlayDevCard(devCard))
			return false;
		return true;
	}

	public boolean canPlayYearOfPlenty(ResourceType resource1, ResourceType resource2, int playerIndex) {
		if (!canPlayDevCard(DevCardType.YEAR_OF_PLENTY, playerIndex))
			return false;
		if (resource1.equals(resource2)) {
			if (bank.getQuantity(resource1) < 2)
				return false;
		} else {
			if (bank.getQuantity(resource2) < 1)
				return false;
			if (bank.getQuantity(resource1) < 1)
				return false;
		}
		return true;
	}

	public boolean canPlayMonument(int playerIndex) {
		if (!canPlayDevCard(DevCardType.MONUMENT, playerIndex))
			return false;
		return true;
	}

	public boolean canPlayMonopoly(int playerIndex) {
		if (!canPlayDevCard(DevCardType.MONOPOLY, playerIndex))
			return false;
		return true;
	}

	public boolean canPlaySoldier(HexLocation hexLocation, int victimIndex, int playerIndex) {
		if (!canPlayDevCard(DevCardType.SOLDIER, playerIndex))
			return false;
		if (victimIndex == playerIndex)
			return false;

		if (getCatanMap().getRobber().equals(hexLocation))
			return false;

		if (victimIndex > 0
				&& getPlayers().get(victimIndex).getResources().getTotalResourceCount() == 0)
			return false;

		return true;
	}

	public boolean canPlayRoadBuild(EdgeLocation location1, EdgeLocation location2, int playerIndex) {
		if (!canPlayDevCard(DevCardType.ROAD_BUILD, playerIndex))
			return false;

		if(getCatanMap().canPlaceRoadAtLocation(location1, playerIndex, "Playing")) {
			IEdgeValue road = new EdgeValue();
			road.setLocation(location1);
			road.setOwner(players.get(playerIndex));
			getCatanMap().getRoads().add(road);
			
			if(getCatanMap().canPlaceRoadAtLocation(location2, playerIndex, "Playing")) {
				getCatanMap().getRoads().remove(road);
				return true;
			}
			getCatanMap().getRoads().remove(road);		
		}

		if(!getCatanMap().canPlaceRoadAtLocation(location2, playerIndex, "Playing"))
			return false;
		
		IEdgeValue road = new EdgeValue();
		road.setLocation(location2);
		road.setOwner(players.get(playerIndex));
		getCatanMap().getRoads().add(road);	
		
		if(getCatanMap().canPlaceRoadAtLocation(location1, playerIndex, "Playing")) {
			getCatanMap().getRoads().remove(road);
			return true;
		}
		getCatanMap().getRoads().remove(road);
		return false;

	}

	@Override
	public boolean canBuyRoadAtLocation(EdgeLocation edgeLocation, int playerIndex) {

		String status = getTurnTracker().getStatus().toLowerCase();
		
		if (!(status.equals("firstround") || status.equals("secondround") || status
				.equals("playing")))
			return false;
		if (!(status.equals("firstround") || status.equals("secondround")))
			if (!players.get(playerIndex).canBuyRoad())
				return false;

		if (!getCatanMap().canPlaceRoadAtLocation(edgeLocation.getNormalizedLocation(),
				playerIndex, status))
			return false;

		return true;
	}

	@Override
	public boolean canBuySettlementAtLocation(VertexLocation vertexLocation, int playerIndex) {
		String status = getTurnTracker().getStatus().toLowerCase();

		if (!(status.equals("firstround") || status.equals("secondround") || status
				.equals("playing")))
			return false;
		if (!(status.equals("firstround") || status.equals("secondround")))
			if (!players.get(playerIndex).canBuySettlement())
				return false;

		if (!getCatanMap().canPlaceSettlementAtLocation(vertexLocation.getNormalizedLocation(),
				playerIndex, status))
			return false;

		return true;
	}

	@Override
	public boolean canBuyCityAtLocation(VertexLocation vertexLocation, int playerIndex) {
		String status = getTurnTracker().getStatus().toLowerCase();

		if (!(status.equals("firstround") || status.equals("secondround") || status
				.equals("playing")))
			return false;
		if (!(status.equals("firstround") || status.equals("secondround")))
			if (!players.get(playerIndex).canBuyCity())
				return false;

		if (!getCatanMap().canPlaceCityAtLocation(vertexLocation.getNormalizedLocation(),
				playerIndex, status))
			return false;

		return true;
	}

	@Override
	public ICatanMap getMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setMap(ICatanMap map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addPlayer(IPlayer player) {

        for(IPlayer otherPlayer : players) {
            if(otherPlayer.getPlayerId() == player.getPlayerId()) {
                otherPlayer.setColor(player.getColor());
                return;
            }
        }
        player.setPlayerIndex(players.size());
        players.add(player);
	}
    @Override
    public boolean canAddPlayer(int playerId, CatanColor color) {
        boolean isInGame = false;
        for(IPlayer otherPlayer : players) {
            if(otherPlayer.getPlayerId() == playerId) {
                isInGame = true;
            }
            else if(otherPlayer.getColor().equals(color)) {
                return false;
            }
        }
        return isInGame || players.size() < 4;
    }

	@Override
	public boolean removePlayer(IPlayer player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public void setName(String title) {
		this.title = title;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

}
