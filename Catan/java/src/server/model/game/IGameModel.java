package server.model.game;

import java.util.List;

import server.model.map.*;
import server.model.message.*;
import shared.definitions.CatanColor;
import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;



/**
 * 
 * @author Jacob Glad
 * Describes the model of a game on the server
 */
public interface IGameModel {
	public IMessageList getChat();
	public void setChat(IMessageList chat);
	
	public IMessageList getLog();
	public void setLog(IMessageList log);
	
	public ICatanMap getMap();
	public void setMap(ICatanMap map);

    /**
    * Retrieves a list of all players in the current game
    * @return The list of all players currently in the game
    */
	public List<IPlayer> getPlayers();

    /**
    * Adds a player to the list of players in the current game
    * @param player The player to be added to the game
    */
	public void addPlayer(IPlayer player);

    boolean canAddPlayer(int playerId, CatanColor color);

    /**
    * Removes a player from the current game
    * @param player The player to be removed from the current game
    * @return Success Status of the removal of the player
    */
	public boolean removePlayer(IPlayer player);
	
	public ITurnTracker getTurnTracker();
	public void setTurnTracker(ITurnTracker turnTracker);
	
	public int getVersion();
	public void setVersion(int version);
	
	public int getWinner();
	public void setWinner(int winner);
	
	public IResourceList getBank();
	public void setBank(IResourceList bank);
	
	public IDevCardList getDeck();
	public void setDeck(IDevCardList deck);
	
	public String getTitle();
	public void setName(String title);
	
	public int getId();
	public void setId(int id);
	boolean canBuyCityAtLocation(VertexLocation vertexLocation, int playerId);
	boolean canBuySettlementAtLocation(VertexLocation vertexLocation, int playerId);
	boolean canBuyRoadAtLocation(EdgeLocation edgeLocation, int playerId);
}
