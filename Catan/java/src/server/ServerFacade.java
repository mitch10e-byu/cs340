package server;

import server.command.ICommandList;
import server.command.game.*;
import server.command.games.*;
import server.command.moves.*;
import server.command.user.*;
import server.command.util.*;
import server.handler.Credentials;
import server.model.game.IGameModel;
import server.model.game.ai.AIList;
import server.result.LoginResult;
import server.result.ModelResult;
import serverProxy.request.*;

import java.util.List;

/**
 * Created by mitch10e on 11/5/14.
 */
public class ServerFacade implements IServerFacade {

    private static ServerFacade singleton;
    public static ServerFacade getSingleton() {
        if(singleton == null) {
            singleton = new ServerFacade();
        }
        return singleton;
    }

    @Override
    public String changeLogLevel(String level) {
        ChangeLogLevelCommand command = new ChangeLogLevelCommand(level);
        return command.execute();
    }

    @Override
    public LoginResult loginUser(LoginUserRequest request) {
        LoginUserCommand command = new LoginUserCommand(request);
        return command.execute();
    }

    @Override
    public LoginResult registerUser(RegisterUserRequest request) {
        RegisterUserCommand command = new RegisterUserCommand(request);
        return command.execute();
    }

    @Override
    public boolean joinGame(JoinGameRequest request, Credentials credientials) {
        JoinGameCommand command = new JoinGameCommand(request, credientials);
        return command.execute();
    }

    @Override
    public ModelResult createGame(CreateGamesRequest request) {
        CreateGameCommand command = new CreateGameCommand(request);
        return command.execute();
    }

    @Override
    public List<IGameModel> listGames(ListGamesRequest request) {
        ListGamesCommand command = new ListGamesCommand(request);
        return command.execute();
    }

    @Override
    public String loadGame(LoadGameRequest request) {
        LoadGameCommand command = new LoadGameCommand(request);
        return command.execute();
    }

    @Override
    public String saveGame(SaveGameRequest request) {
        SaveGameCommand command = new SaveGameCommand(request);
        return command.execute();
    }

    @Override
    public String addAI(AddAIRequest request) {
        AddAICommand command = new AddAICommand(request);
        return command.execute();
    }

    @Override
    public AIList listAI(ListAIRequest request) {
        ListAICommand command = new ListAICommand(request);
        return command.execute();
    }

    @Override
    public ICommandList getCommands(GetGameCommandsRequest request) {
        ListCommandsCommand command = new ListCommandsCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel postCommands(PostGameCommandsRequest request) {
        RunCommandsCommand command = new RunCommandsCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel getGameModel(GameModelRequest request) {
        ModelCommand command = new ModelCommand(request);
        return command.execute();
    }

    @Override
    public String resetGame(ResetGameRequest request) {
        ResetCommand command = new ResetCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel acceptTrade(AcceptTradeRequest request) {
        AcceptTradeCommand command = new AcceptTradeCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel buildCity(BuildCityRequest request) {
        BuildCityCommand command = new BuildCityCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel buildRoad(BuildRoadRequest request) {
        BuildRoadCommand command = new BuildRoadCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel buildSettlement(BuildSettlementRequest request) {
        BuildSettlementCommand command = new BuildSettlementCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel buyDevCard(BuyDevCardRequest request) {
        BuyDevCardCommand command = new BuyDevCardCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel discardCards(DiscardCardsRequestServer request) {
        DiscardCardsCommand command = new DiscardCardsCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel finishTurn(FinishTurnRequest request) {
        FinishTurnCommand command = new FinishTurnCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel maritimeTrade(MaritimeTradeRequest request) {
        MaritimeTradeCommand command = new MaritimeTradeCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel monopoly(MonopolyRequest request) {
        MonopolyCommand command = new MonopolyCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel monument(MonumentRequest request) {
        MonumentCommand command = new MonumentCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel offerTrade(OfferTradeRequestServer request) {
        OfferTradeCommand command = new OfferTradeCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel roadBuilding(RoadBuildingRequest request) {
        RoadBuildingCommand command = new RoadBuildingCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel robPlayer(RobPlayerRequest request) {
        RobPlayerCommand command = new RobPlayerCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel rollNumber(RollNumberRequest request) {
        RollNumberCommand command = new RollNumberCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel sendChat(SendChatRequest request) {
        SendChatCommand command = new SendChatCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel soldier(SoldierRequest request) {
        SoldierCommand command = new SoldierCommand(request);
        return command.execute();
    }

    @Override
    public IGameModel yearOfPlenty(YearOfPlentyRequest request) {
        YearOfPlentyCommand command = new YearOfPlentyCommand(request);
        return command.execute();
    }

}
