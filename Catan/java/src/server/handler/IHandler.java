package server.handler;

import com.sun.net.httpserver.HttpHandler;

/**
 * Wrapper Class for HttpHandler: Handle an HttpServer Context
 */
public interface IHandler extends HttpHandler {

}
