package server.handler.games;

import com.sun.net.httpserver.HttpExchange;
import server.ServerFacade;
import server.handler.IHandler;
import server.handler.ServerTranslator;
import server.logging.Logger;
import server.result.SuccessResult;
import serverProxy.request.LoadGameRequest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/**
 * Server Handler to process /games/load
 */
public class LoadGameHandler implements IHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Logger.getSingleton().info("Load Game Handler");
        OutputStream os = httpExchange.getResponseBody();
        SuccessResult result = new SuccessResult();
        if(!httpExchange.getRequestMethod().equals("POST")) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            result.setResult("Bad Request Method: " + httpExchange.getRequestMethod());
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            ServerTranslator translator = new ServerTranslator(httpExchange.getRequestBody());
            LoadGameRequest request = translator.getLoadGameRequest();
            result.setResult(ServerFacade.getSingleton().loadGame(request));
        }

        os.write(((String)result.getResult()).getBytes());
        os.close();
        httpExchange.getResponseBody().close();
    }
}
