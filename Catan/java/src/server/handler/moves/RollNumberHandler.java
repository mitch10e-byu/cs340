package server.handler.moves;

import com.sun.net.httpserver.HttpExchange;
import server.ServerFacade;
import server.handler.IHandler;
import server.handler.ServerTranslator;
import server.logging.Logger;
import server.result.IResult;
import server.result.ModelResult;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.RollNumberRequest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/**
 * Server Handler to process /moves/rollNumber
 */
public class RollNumberHandler implements IHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Logger.getSingleton().info("Accept Trade Handler");
        OutputStream os = httpExchange.getResponseBody();
        String result = "";
        if(!httpExchange.getRequestMethod().equals("POST")) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            result = "Bad Request Method: " + httpExchange.getRequestMethod();
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            ServerTranslator translator = new ServerTranslator(httpExchange.getRequestBody());
            RollNumberRequest request = translator.getRollNumberRequest();
            IResult gameModel = new ModelResult(ServerFacade.getSingleton().rollNumber(request));
            result = translator.makeGameModelResponse(gameModel).toString();
        }

        os.write(result.toString().getBytes());
        os.close();
        httpExchange.getResponseBody().close();
    }
}
