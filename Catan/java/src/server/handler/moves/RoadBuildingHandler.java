package server.handler.moves;

import com.sun.net.httpserver.HttpExchange;
import server.ServerFacade;
import server.handler.IHandler;
import server.handler.ServerTranslator;
import server.logging.Logger;
import server.result.IResult;
import server.result.ModelResult;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.RoadBuildingRequest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/**
 * Server Handler to process /moves/Road_Building
 */
public class RoadBuildingHandler implements IHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Logger.getSingleton().info("Accept Trade Handler");
        OutputStream os = httpExchange.getResponseBody();
        String result = "";
        if(!httpExchange.getRequestMethod().equals("POST")) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            result = "Bad Request Method: " + httpExchange.getRequestMethod();
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            ServerTranslator translator = new ServerTranslator(httpExchange.getRequestBody());
            RoadBuildingRequest request = translator.getRoadBuildingRequest();
            IResult gameModel = new ModelResult(ServerFacade.getSingleton().roadBuilding(request));
            result = translator.makeGameModelResponse(gameModel).toString();
        }

        os.write(result.toString().getBytes());
        os.close();
        httpExchange.getResponseBody().close();
    }
}
