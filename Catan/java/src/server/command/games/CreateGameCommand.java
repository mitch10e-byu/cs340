package server.command.games;

import server.command.ICommand;
import server.result.IServerModel;
import server.result.ServerModel;
import server.model.game.IGameModel;
import server.result.ModelResult;
import serverProxy.request.CreateGamesRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class CreateGameCommand implements ICommand {

    private CreateGamesRequest request;
    private ModelResult result;
    public CreateGameCommand(CreateGamesRequest request) {
        IServerModel serverModel = ServerModel.getSingleton();
        IGameModel gameModel = serverModel.addGame(request.getName(), request.isRandomPorts(), request.isRandomNumbers(), request.isRandomPorts());
        result = new ModelResult(gameModel);
    }

    @Override
    public ModelResult execute() {
        return result;
    }
}
