package server.command.games;

import server.command.ICommand;
import server.result.ServerModel;
import server.model.game.IGameModel;
import serverProxy.request.ListGamesRequest;

import java.util.List;

/**
 * Created by mitch10e on 11/5/14.
 */
public class ListGamesCommand implements ICommand {

    private ListGamesRequest request;
    private List<IGameModel> gameList;
    public ListGamesCommand(ListGamesRequest request) {
        this.request = request;
        gameList = ServerModel.getSingleton().getGames();
    }

    @Override
    public List<IGameModel> execute() {
        return gameList;
    }
}
