package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.RoadBuildingRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class RoadBuildingCommand implements ICommand {
    private RoadBuildingRequest request;

    public RoadBuildingCommand(RoadBuildingRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
