package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.BuildCityRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class BuildCityCommand implements ICommand {
    private BuildCityRequest request;

    public BuildCityCommand(BuildCityRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
