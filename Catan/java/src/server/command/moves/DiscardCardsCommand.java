package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.DiscardCardsRequest;
import serverProxy.request.DiscardCardsRequestServer;

/**
 * Created by mitch10e on 11/5/14.
 */
public class DiscardCardsCommand implements ICommand {
    private DiscardCardsRequestServer request;

    public DiscardCardsCommand(DiscardCardsRequestServer request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
