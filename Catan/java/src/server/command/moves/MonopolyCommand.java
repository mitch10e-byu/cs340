package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.MonopolyRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class MonopolyCommand implements ICommand {
    private MonopolyRequest request;

    public MonopolyCommand(MonopolyRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
