package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.SendChatRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class SendChatCommand implements ICommand {
    private SendChatRequest request;

    public SendChatCommand(SendChatRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
