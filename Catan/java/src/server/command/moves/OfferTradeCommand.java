package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.OfferTradeRequest;
import serverProxy.request.OfferTradeRequestServer;

/**
 * Created by mitch10e on 11/5/14.
 */
public class OfferTradeCommand implements ICommand {
    private OfferTradeRequestServer request;

    public OfferTradeCommand(OfferTradeRequestServer request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
