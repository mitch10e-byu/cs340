package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.BuildRoadRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class BuildRoadCommand implements ICommand {
    private BuildRoadRequest request;

    public BuildRoadCommand(BuildRoadRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
