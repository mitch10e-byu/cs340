package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.RollNumberRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class RollNumberCommand implements ICommand {
    private RollNumberRequest request;

    public RollNumberCommand(RollNumberRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
