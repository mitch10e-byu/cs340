package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.FinishTurnRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class FinishTurnCommand implements ICommand {
    private FinishTurnRequest request;

    public FinishTurnCommand(FinishTurnRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
