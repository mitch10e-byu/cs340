package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.GameModelRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class AcceptTradeCommand implements ICommand {
    private AcceptTradeRequest request;

    public AcceptTradeCommand(AcceptTradeRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
