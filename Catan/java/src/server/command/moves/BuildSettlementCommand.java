package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.BuildSettlementRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class BuildSettlementCommand implements ICommand{
    private BuildSettlementRequest request;

    public BuildSettlementCommand(BuildSettlementRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
