package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.YearOfPlentyRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class YearOfPlentyCommand implements ICommand {
    private YearOfPlentyRequest request;

    public YearOfPlentyCommand(YearOfPlentyRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
