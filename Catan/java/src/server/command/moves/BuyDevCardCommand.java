package server.command.moves;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.AcceptTradeRequest;
import serverProxy.request.BuyDevCardRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class BuyDevCardCommand implements ICommand {
    private BuyDevCardRequest request;

    public BuyDevCardCommand(BuyDevCardRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
