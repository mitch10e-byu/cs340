package server.command.game;

import server.command.ICommand;
import server.model.game.IGameModel;
import serverProxy.request.GameModelRequest;

/**
 * Created by mitch10e on 11/5/14.
 */
public class ModelCommand implements ICommand {

    private GameModelRequest request;

    public ModelCommand(GameModelRequest request) {
        this.request = request;
    }

    @Override
    public IGameModel execute() {
        return null;
    }
}
