package server.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import server.handler.Credentials;
import server.model.game.*;
import shared.definitions.CatanColor;

public class ServerModel implements IServerModel {
	private static ServerModel singleton;
	public static ServerModel getSingleton() {
		if(singleton == null) {
			singleton = new ServerModel();
		}
		return singleton;
	}
	Map<String, IUser> users;
	List<IGameModel> games;
	public ServerModel() {
		users = new HashMap<>();
		games = new ArrayList<IGameModel>();
	}
	@Override
	public List<IGameModel> getGames() {
		return games;
	}

	@Override
	public IGameModel addGame(String name, boolean randomTiles, boolean randomNumbers, boolean randomPorts) {
		int id = games.size();
		IGameModel game = new GameModel(name, id, randomTiles, randomNumbers, randomPorts);
		games.add(game);
		return game;
	}

	@Override
	public boolean removeGame(IGameModel game) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<IUser> getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IUser addUser(String username, String password) {
		IUser user = new User();
		user.setName(username);
		user.setPassword(password);
		user.setId(users.size());
		users.put(username, user);
		return user;
	}

	@Override
	public boolean canAddUser(String username, String password) {
		if(users.containsKey(username))
			return false;
		String regex = "[a-zA-Z1-9\\-_]+";
		if(!username.matches(regex))
			return false;
		if(username.length() < 3 || username.length() > 7) 
			return false;
		if(!password.matches(regex))
			return false;
		if(password.length() < 5)
			return false;
		return true;
	}
	@Override
	public IUser getUser(String username) {
		return users.get(username);
	}

    public boolean checkCredentials(Credentials credentials) {
        IUser user = users.get(credentials.getName());
        if(user == null)
            return false;
        if(!user.getPassword().equals(credentials.getPassword()))
            return false;
        if(user.getId() != credentials.getPlayerId())
            return false;

        return true;
    }

    public boolean canJoinGame(int gameId, int playerId, CatanColor color) {
        if(gameId < 0)
            return false;
        if(gameId >= games.size())
            return false;
        IGameModel game = games.get(gameId);
        if(!game.canAddPlayer(playerId, color)){
            return false;
        }
        return true;
    }
    public void joinGame(int gameId, int playerId, CatanColor color) {

        for(Map.Entry<String, IUser> entry: users.entrySet()) {
            IUser user = entry.getValue();
            if(user.getId() == playerId) {
                IPlayer player = new Player(user.getName(), user.getId(), color);
                games.get(gameId).addPlayer(player);
                return;
            }
        }
    }

}
