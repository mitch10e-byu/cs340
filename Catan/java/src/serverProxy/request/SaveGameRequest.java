package serverProxy.request;

import serverProxy.IRequest;
import serverProxy.IResponse;

/**
 * Created by mitch10e on 11/5/14.
 */
public class SaveGameRequest implements IRequest {
    @Override
    public String getEndpoint() {
        return "/games/save";
    }

    @Override
    public String getRequestMethod() {
        return "POST";
    }

    @Override
    public String getBody() {
        return null;
    }

    @Override
    public IResponse getDefaultResponse() {
        return null;
    }
}
