package serverProxy.request;

import serverProxy.IRequest;
import serverProxy.IResponse;

/**
 * Created by mitch10e on 11/5/14.
 */
public class LoadGameRequest implements IRequest {
    @Override
    public String getEndpoint() {
        return "/games/load";
    }

    @Override
    public String getRequestMethod() {
        return "POST";
    }

    @Override
    public String getBody() {
        return null;
    }

    @Override
    public IResponse getDefaultResponse() {
        return null;
    }
}
